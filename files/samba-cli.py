#!/usr/bin/env python3
import sys
import time
import random
import logging
import subprocess
from os import getenv
from os.path import abspath, dirname, join, expanduser

import click

log = logging.getLogger(__name__)
HERE = dirname(abspath(__file__))

SAMBA_REPO_ROOT = expanduser(getenv('SAMBA_REPO_ROOT', '~/samba'))
SAMBA_REPO_BIN = join(SAMBA_REPO_ROOT, 'bin/python')
sys.path.insert(0, SAMBA_REPO_BIN)

SAMBA_USERNAME = getenv('SAMBA_USERNAME', 'Administrator')
SAMBA_PASSWORD = getenv('SAMBA_PASSWORD', 'Password01@')

SAMBA_INSTALL_DIR = getenv('SAMBA_INSTALL_DIR', '/usr/local/samba')
SAMBA_DATA_DIR = getenv('SAMBA_DATA_DIR', '/usr/local/samba')

SAMBA_TOOL = join(SAMBA_INSTALL_DIR, 'bin/samba-tool')
SAMBA_LDB_PATH = join(SAMBA_DATA_DIR, 'private/sam.ldb')
SAMBA_CONF_PATH = join(SAMBA_DATA_DIR, 'etc/smb.conf')

import ldb
from samba.samdb import SamDB
from samba.auth import system_session
from samba.param import LoadParm
from samba.credentials import Credentials

session = system_session()

lp = LoadParm()
lp.load(SAMBA_CONF_PATH)
# lp.load_default()
log.debug('configfile: %s', lp.configfile)
log.debug('samdb_url: %s', lp.samdb_url())

creds = Credentials()
creds.guess(lp)

db = SamDB(session_info=session, lp=lp, credentials=creds)
db.connect(url=SAMBA_LDB_PATH)


def ftime(f):
    def wrapper(*args, **kwargs):
        t0 = time.time()
        try:
            return f(*args, **kwargs)
        finally:
            log.info('%s took %.2f s', f.__name__, time.time() - t0)
    return wrapper


def check_output(cmd, cwd=None):
    log.info('cmd:\n\n\t%s\n\n', cmd)
    output = subprocess.check_output(
        cmd, shell=True, cwd=cwd, stderr=subprocess.STDOUT,
    ).decode('utf8')
    log.info('cmd output: \n\n\t%s\n\n', output)
    return output


def samba_tool(subcmd, args):
    cmd = '{} {} {} --configfile {}'.format(SAMBA_TOOL, subcmd, args, SAMBA_CONF_PATH)
    return check_output(cmd)

@ftime
def samba_tool_user_add(name, password=None):
    try:
        return samba_tool('user', 'add {} {}'.format(name, password or SAMBA_PASSWORD))
    except subprocess.CalledProcessError as exc:
        if 'already in use' in exc.output or 'already exists' in exc.output:
            log.info('%s already exists, error ignored:\n%s', name, exc.output)
        else:
            raise

def samba_tool_user_exist(name):
    """Use enable to check user exist or not"""
    try:
        samba_tool('user', 'enable {}'.format(name))
        return True
    except subprocess.CalledProcessError as exc:
        if 'Unable to find account' in exc.output:
            log.info('%s not exists:\n%s', name, exc.output)
            return False
        else:
            raise

@ftime
def samba_tool_group_add(name):
    try:
        return samba_tool('group', 'add {}'.format(name))
    except subprocess.CalledProcessError as exc:
        if 'already in use' in exc.output:
            log.info('%s already exists, error ignored: \n%s', name, exc.output)
        else:
            raise

def samba_tool_group_exist(name):
    try:
        return samba_tool('group', 'addmembers {} i-am-not-real'.format(name))
    except subprocess.CalledProcessError as exc:
        if 'Unable to find group' in exc.output:
            log.info('%s not exists: \n%s', name, exc.output)
            return False
        elif 'Unable to find' in exc.output:
            # can not find user, which means group exist
            return True
        else:
            raise

@ftime
def samba_tool_group_addmembers(group, users):
    return samba_tool('group', 'addmembers {} {}'.format(group, ','.join(users)))

def get_dn(name, ou='Users'):
    return 'CN={},CN={},{}'.format(name, ou, db.domain_dn()),


def add_object(objectClass='user', ou=None, id=0):
    name = '{}-{}'.format(objectClass, id)
    ou = ou or objectClass.title() + 's'
    dn = get_dn(name, ou)
    msg = {
        'objectClass': objectClass,
        'samAccountName': name,
        'dn': dn,
    }
    try:
        db.add(msg)
        log.debug('add %s %s', objectClass, name)
        return dn
    except ldb.LdbError as exc:
        if exc[0] == 68:  # ignore if exist
            log.warn('%s already exists, ignore', name)
        else:
            raise


def populate_db():
    count = 1000

    for id in range(count):
        add_object(id=id)

    for id in range(count/10):
        add_object(id=id, objectClass='group', ou='Users')

    group_dn = get_dn('group-0')
    lines = [
        'dn: %s' % group_dn,
        'changetype: modify',
    ]
    for id in range(count):
        name = 'user-%s' % id
        user_dn = get_dn(name)
        lines.extend([
            'add: member',
            'member: %s' % user_dn,
        ])
    db.modify_ldif('\n'.join(lines))

    # add half users to group-1
    group_dn = get_dn('group-1')
    lines = [
        'dn: %s' % group_dn,
        'changetype: modify',
    ]
    for id in range(count/2):
        name = 'user-%s' % id
        user_dn = get_dn(name)
        lines.extend([
            'add: member',
            'member: %s' % user_dn,
        ])
    db.modify_ldif('\n'.join(lines))


@click.group()
@click.option("-v", "--verbose", default=False, is_flag=True, help='verbose log')
def cli(verbose):
    fmt = '%(asctime)s - %(levelname)s - %(module)s - %(message)s'
    level = 'DEBUG' if verbose else 'ERROR'
    logging.basicConfig(level=level, format=fmt)


@cli.command()
@click.option("-n", "--num-users", type=int, default=1000, help='num of users')
@click.option("--num-groups", type=int, help='num of groups, default to num_users/10')
@click.option("--num-groups-per-user", type=int, default=3, help='num of groups per user')
def populate(num_users, num_groups, num_groups_per_user):
    if num_groups is None:
        num_groups = int(num_users / 10)

    users = ['user-{}'.format(i) for i in range(num_users)]
    groups = ['group-{}'.format(i) for i in range(num_groups)]

    if samba_tool_user_exist(users[-1]):
        log.info('%s already exists, skip user generate', users[-1])
    else:
        for name in users:
            samba_tool_user_add(name)

    if samba_tool_group_exist(groups[-1]):
        log.info('%s already exists, skip group generate', groups[-1])
    else:
        for name in groups:
            samba_tool_group_add(name)

    for user in users:
        for _ in range(num_groups_per_user):
            group = random.choice(groups)
            samba_tool_group_addmembers(group, [user])


@cli.command()
def ipython():
    from IPython import embed
    embed()


if __name__ == '__main__':
    cli(auto_envvar_prefix='SAMBA')
