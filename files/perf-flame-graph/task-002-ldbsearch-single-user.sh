#!/bin/bash -x

# -H and -U are required for ldbsearch
for i in {1..50}
do
/usr/local/samba/bin/ldbsearch -H ldap://$DC -UAdministrator%Password01@ "(cn=STGU-0-$i)"
done
