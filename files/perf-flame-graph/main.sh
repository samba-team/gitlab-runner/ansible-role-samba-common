#!/bin/bash

set -xue

export DC=$(hostname)

FLAME_GRAPH=../FlameGraph

rm -f *.svg

for script in `ls task-*.sh`;
do
  perf record -F 99 -a -g -- ./$script
  perf script | $FLAME_GRAPH/stackcollapse-perf.pl > out.perf-folded
  $FLAME_GRAPH/flamegraph.pl --title "$script" out.perf-folded > ./$script.svg
done

rm -f perf.data perf.data.old out.perf-folded
